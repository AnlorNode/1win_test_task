
const koaBody = require('koa-body');
const { searchBooks, addBooks, changeoverBooks } = require('../middlewares');

module.exports = (router) => {
  router
    .post('/books/add', koaBody(), async (ctx) => {
      await addBooks.setBooks(ctx)
    })
    .get('/books', async (ctx) => {
      await searchBooks.getBooks(ctx)
    })
    .put('/books', koaBody(), async (ctx) => {
      await changeoverBooks.changeBooks(ctx)
    })
};