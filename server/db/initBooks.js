const mysql = require('mysql2/promise');
const moment = require('moment');
const casual = require('casual');
const { conectDB } = require('../config.json');


module.exports = async () => {
  const connection = await mysql.createConnection(conectDB)
  let execute
  try {
    execute = await connection.execute(
      "SELECT id from `books` LIMIT 1;"
    )
    connection.end()
  } catch (error) {
    if (error.code === 'ER_NO_SUCH_TABLE') {
      try {
        let sql = ''
        await connection.execute(
          "CREATE TABLE IF NOT EXISTS `books` (" +
          "`id` INT UNSIGNED AUTO_INCREMENT," +
          "`title` TEXT NOT NULL," +
          "`date` DATE NOT NULL," +
          "`author` TEXT NOT NULL," +
          "`description` TEXT NOT NULL," +
          "`image` TEXT NOT NULL," +
          "PRIMARY KEY (`id`)" +
          ");"
        )
          sql += 'INSERT INTO books (`title`, `date`, `author`, `description`, `image`) VALUES '
          for (let index = 0; index < 10000/* 0 TODO*/; index += 1) {
            casual.define('point', () => ({
              x: Math.random(),
              y: Math.random()
            })
            );
            sql += `('${casual.title}', '${moment().format('YYYY-MM-DD')}','${casual.name}', '${casual.description}', '${casual.uuid}') ,`
          }
          sql += `('title', '${moment().format('YYYY-MM-DD')}','${casual.name}', '${casual.description}', '${casual.uuid}') ; `

        await connection.beginTransaction();
        await connection.query(sql);
        await connection.commit();
      } catch (error) {
        console.error(error)
        await connection.rollback();
      }

    }
  }


  return execute
}
