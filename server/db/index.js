const getBooks = require('./getBooks');
const initBooks = require('./initBooks');
const setBooks = require('./setBooks');
const removeBooks = require('./removeBooks');
const changeBooks = require('./changeBooks');

module.exports = {
  getBooks,
  initBooks,
  setBooks,
  removeBooks,
  changeBooks,
};