const mysql = require('mysql2/promise');
const { conectDB } = require('../config.json');
const { dbSql: { sqlSet } } = require('../modules');

module.exports = async ({ id, change }) => {
  try {
    let sqlObj = {
      sqlArr: [],
      sqlStr: 'UPDATE books SET ',
    }
    if (change) {
      const arr = Object.keys(change)
      const keys = arr.map((key, i) => {
        if (arr.length > i + 1) {
          return sqlSet(`${key} = '${change[key]}',`, sqlObj)
        }
        return sqlSet(`${key} = '${change[key]}'`, sqlObj)
      })
    }
    const connection = await mysql.createConnection(conectDB)
    const { sqlStr, sqlArr } = sqlObj
    const execute = await connection.execute(sqlStr, sqlArr)
    connection.end()
    return { execute }
  } catch (error) {
    console.error(error)
    return { error }
  }
}