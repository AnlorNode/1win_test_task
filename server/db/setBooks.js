const mysql = require('mysql2/promise');
const moment = require('moment');
const { conectDB } = require('../config.json');

module.exports = async ({ title, author, description, image }) => {
  try {
    const connection = await mysql.createConnection(conectDB)
    const execute = await connection.execute(
      "INSERT INTO `books`" +
      "(`title`, `date`, `author`, `description`, `image`)" +
      "VALUES (?, ?, ?, ?, ?);", [
        title,
        moment().format('YYYY-MM-DD'),
        author,
        description,
        image
      ]
    )
    connection.end()
    return { execute }
  } catch (error) {
    console.error(error)
    return { error }
  }
}
