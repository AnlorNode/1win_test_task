const mysql = require('mysql2/promise');
const { conectDB } = require('../config.json');
const { dbSql: { sqlSet } } = require('../modules');

module.exports = async ({ sort, limit, offset, search }) => {
  try {
    let sqlObj = {
      sqlArr: [],
      sqlStr: 'SELECT * from books ',
    }

    const connection = await mysql.createConnection(conectDB)
    if (search) {
      const keys = Object.keys(search)[0]
      sqlSet(`WHERE ${keys} LIKE`, sqlObj, `%${search[keys]}%`)
    }
    if (sort) {
      const keys = Object.keys(sort)[0]
      sqlSet(`ORDER BY ${keys} ${sort[keys]}`, sqlObj)
    }
    if (limit) {
      sqlSet(`LIMIT ${limit}`, sqlObj)
    } else {
      sqlSet(`LIMIT 20 `, sqlObj)
    }
    if (offset) {
      sqlSet(`OFFSET ${offset}`, sqlObj)
    }
    const { sqlStr, sqlArr } = sqlObj
    const execute = await connection.execute(sqlStr, sqlArr)
    connection.end()
    return { execute }
  } catch (error) {
    console.error(error)
    return { error }
  }
}