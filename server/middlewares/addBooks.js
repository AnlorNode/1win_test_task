const { setBooks } = require('../db');

module.exports = {
  setBooks: async (ctx) => {
    const body = ctx.request.body
    const { error, execute } = await setBooks(body)
    if (error) {
      ctx.status = 204;
      return
    }
    ctx.status = 201;
    ctx.body = 'ok';
  }
};
