const { removeBooks } = require('../db');

module.exports = {
  getBooks: async () => {
    const removeBook = await removeBooks()
    return removeBook
  }
};
