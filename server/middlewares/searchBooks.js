const qs = require("qs");
const { getBooks } = require('../db');

module.exports = {
  getBooks: async (ctx) => {
    let querystring = qs.parse(ctx.querystring)
    const { error, execute: [data] } = await getBooks(querystring)
    if (error) {
      ctx.status = 204;
      return
    }
    ctx.status = 200;
    ctx.body = data;
  }
};
