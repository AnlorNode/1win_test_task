const creatureBooks = require('./creatureBooks');
const deleteBooks = require('./deleteBooks');
const searchBooks = require('./searchBooks');
const addBooks = require('./addBooks');
const changeoverBooks = require('./changeoverBooks');

module.exports = {
  creatureBooks,
  deleteBooks,
  searchBooks,
  addBooks,
  changeoverBooks,
};