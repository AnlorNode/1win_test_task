const { initBooks } = require('../db');

module.exports = {
  initBooks: async () => {
    const initBook = await initBooks()
    return initBook
  }
};
