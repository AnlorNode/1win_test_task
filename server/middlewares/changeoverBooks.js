const qs = require("qs");
const { changeBooks } = require('../db');

module.exports = {
  changeBooks: async (ctx) => {
    const body = ctx.request.body
    const { error, execute: [data] } = await changeBooks(body)
    if (error) {
      ctx.status = 204;
      return
    }
    ctx.status = 200;
    ctx.body = data;
  }
};
