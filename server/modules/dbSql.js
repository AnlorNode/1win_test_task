
module.exports = {
  sqlSet: (dat, sqlObj, arr) => {
    if (arr) {
      sqlObj.sqlArr.push(`${arr}`)
      sqlObj.sqlStr += `${dat} ? `
      return
    }
    sqlObj.sqlStr += `${dat} `
  }
}