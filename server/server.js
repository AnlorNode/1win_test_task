const Koa = require('koa');
const { creatureBooks } = require('./middlewares');
const { server } = require('./config.json');
const router = require('./router');
const app = new Koa();
(async () => {
  console.info('initialization DB')
    await creatureBooks.initBooks()
})();
app.use(router.routes());

app.listen(server.port);