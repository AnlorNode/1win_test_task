

## Http-сервер API Koa2 REST



Пример ответа:

```JSON
[{
        "id": 1,
        "title": "Aspernatur quo",
        "date": "2019-06-19T21:00:00.000Z",
        "author": "Ms. Herminia Parisian",
        "description": "Perferendis perferendis id ratione. Quo eligendi nostrum rem nihil voluptatum ullam qui pariatur repudiandae. Sequi facere quia qui officia odit. Eveniet veritatis minima quasi.",
        "image": "b5e5040d-2fa4-4b42-a197-2299d41d9362"
    },
    {
        "id": 2,
        "title": "Veniam minus consequatur",
        "date": "2019-06-19T21:00:00.000Z",
        "author": "Mr. Elisa Marquardt",
        "description": "Quo labore reiciendis. Quod aliquid enim voluptates. Doloremque quis iste modi maxime iusto voluptatem.",
        "image": "81973ba4-e8f2-4fb1-8df1-539e3501d186"
    },
    {
        "id": 3,
        "title": "Nostrum officia eos",
        "date": "2019-06-19T21:00:00.000Z",
        "author": "Miss Jana Kuhic",
        "description": "Officia deserunt voluptas consectetur. Sunt animi voluptatibus qui corporis voluptas quos perferendis iste aut. Animi asperiores consectetur doloribus sunt ut exercitationem cumque rerum quos. Minima in hic rerum eveniet voluptatibus tenetur in velit.",
        "image": "c0eaca5e-be5f-4adf-9794-460820b475d5"
    },]
```

### Запуск сервера

Для запуска `Http-сервер API Koa2 REST` потребуется [docker](https://docs.docker.com/docker-for-windows/install/), [docker-compose](https://docs.docker.com/compose/install/) [node](https://nodejs.org) . Параметры для подключения приложения к базе данных находятся в файле [config.json](https://gitlab.com/AnlorNode/1win_test_task/tree/master/server/config.json).

После клонирования репозитория перейдите в директорию проекта и в терминале ввидите:

```bash
 docker-compose up && npm i && npm run dev
```

Старт сервера, по умолчанию прослушивает `localhost`, порт `3000`



### Получение списка книг


-  **GET** - 

```
http://localhost:3000/books 
and
http://localhost:3000/books?sort[date]=asc&limit=3&offset=0&search[title]=title
```
### Добавление новой книги

-  **POST** -

http://localhost:3000/books/add
body:{
        "title": "title",
        "author": "author",
        "description": "description",
        "image": "image"
    }

### Изменение существующей книги 

-  **PUT** - 

http://localhost:3000/books
body:{
        "id": 1,
        "change": {
        	"title":"new title"
        }
    }
